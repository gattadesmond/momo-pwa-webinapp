var routes = [
  // {
  //   path: "/",
  //   url: "./pages/home.html",
  // },
  // {
  //   path: "/",
  //   url: "./index.html"
  // },
  {
    path: "/",
    master: true,
    componentUrl: "./pages/home.html",
    on: {
      pageAfterIn: function (e, page) {
        // app.views.main.router.clearPreviousHistory();
      },
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/heodihoc/",
    master: true,
    componentUrl: "./pages/heodihoc.html",
    on: {
      pageAfterIn: function (e, page) {
        // app.views.main.router.clearPreviousHistory();
      },
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/service-page/",
    master: true,
    componentUrl: "./pages/service-page.html",
    on: {
      pageAfterIn: function (e, page) {
        // app.views.main.router.clearPreviousHistory();
      },
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/cashback/",
    master: true,
    componentUrl: "./pages/cashback.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/donation-ungho/",
    master: true,
    componentUrl: "./pages/donation-ungho.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/donation-ungho-rule/",
    master: true,
    componentUrl: "./pages/donation-ungho-rule.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/donation-ungho-manage/",
    master: true,
    componentUrl: "./pages/donation-ungho-manage.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/cashback-search/",
    master: true,
    componentUrl: "./pages/cashback-search.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/cashback2/",
    master: true,
    componentUrl: "./pages/cashback2.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/referral/",
    master: true,
    componentUrl: "./pages/referral.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/referral-gift/",
    master: true,
    componentUrl: "./pages/referral-gift.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/referral-friends/",
    master: true,
    componentUrl: "./pages/referral-friends.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/quyengop/",
    master: true,
    componentUrl: "./pages/quyengop.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/quyengop-lichsu/",
    master: true,
    componentUrl: "./pages/quyengop-lichsu.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/quyengop-empty/",
    master: true,
    componentUrl: "./pages/quyengop-empty.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/quyengop-hoanthanh/",
    master: true,
    componentUrl: "./pages/quyengop-hoanthanh.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/cashback2-single/",
    master: true,
    componentUrl: "./pages/cashback2-single.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/cashback-single/",
    master: true,
    componentUrl: "./pages/cashback-single.html",
    on: {
      pageInit: function (e, page) {},
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/htu/",
    master: true,
    componentUrl: "./pages/htu.html",
    on: {
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/htuBank/",
    master: true,
    componentUrl: "./pages/htuBank.html",
    on: {
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/location-select/",
    master: true,
    componentUrl: "./pages/location-select.html",
    on: {
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/merchant/",
    master: true,
    componentUrl: "./pages/merchant.html",
    on: {
      pageAfterIn: function (e, page) {},
      beforeEnter: function (routeTo, routeFrom, resolve, reject) {},
    },
  },
  {
    path: "/single/",
    componentUrl: "./pages/single.html",
    on: {
      pageInit: function (e, page) {
        DonaGallery();
        app.preloader.show();
      },
      pageAfterIn: function (e, page) {
        // page has left the view
        app.preloader.hide();
      },
    },
  },
  {
    path: "/countdown/",
    componentUrl: "./pages/countdown.html",
    on: {
      pageInit: function (e, page) {
        DonaGallery();
        app.preloader.show();
      },
      pageAfterIn: function (e, page) {
        // page has left the view
        app.preloader.hide();
      },
    },
  },
  {
    path: "/sale/",
    componentUrl: "./pages/sale.html",
    on: {
      pageInit: function (e, page) {
        app.preloader.show();

        var saleMoreCon = Array.from(
          document.querySelectorAll(".js-sale-more")
        );
        saleMoreCon.forEach(function (e) {
          contentLoadMore(e);
        });
      },
      pageAfterIn: function (e, page) {
        // page has left the view
        app.preloader.hide();
      },
    },
  },
  {
    path: "/quiz/",
    componentUrl: "./pages/quiz.html",
    on: {
      pageInit: function (e, page) {
        app.preloader.show();
      },
      pageAfterIn: function (e, page) {
        // page has left the view
        app.preloader.hide();
      },
    },
  },

  {
    path: "/quiz-2/",
    componentUrl: "./pages/quiz-2.html",
    on: {
      pageInit: function (e, page) {
        app.preloader.show();
      },
      pageAfterIn: function (e, page) {
        // page has left the view
        app.preloader.hide();
      },
    },
  },

  {
    path: "/sale-styleguide/",
    componentUrl: "./pages/sale-styleguide.html",
    on: {
      pageInit: function (e, page) {
        app.preloader.show;
      },
      pageAfterIn: function (e, page) {
        app.preloader.hide();
      },
    },
  },
  {
    path: "/sale-list/",
    url: "./pages/sale-list.html",
    on: {
      pageInit: function (e, page) {
        app.preloader.show();
      },
      pageAfterIn: function (e, page) {
        // page has left the view
        app.preloader.hide();
      },
    },
  },
  {
    path: "/sale2/",
    url: "./pages/sale2.html",
    on: {
      pageInit: function (e, page) {
        app.preloader.show();

        var saleMoreCon = Array.from(
          document.querySelectorAll(".js-sale-more")
        );
        saleMoreCon.forEach(function (e) {
          contentLoadMore(e);
        });
      },
      pageAfterIn: function (e, page) {
        // page has left the view
        app.preloader.hide();
      },
    },
  },
  {
    path: "/xacnhan-fail/",
    url: "./pages/xacnhan-fail.html",
    on: {
      pageInit: function (e, page) {
        app.preloader.show();
      },
      pageAfterIn: function (e, page) {
        // page has left the view
        app.preloader.hide();
      },
    },
  },
  {
    path: "/xacnhan-success/",
    url: "./pages/xacnhan-success.html",
    on: {
      pageInit: function (e, page) {
        app.preloader.show();
      },
      pageAfterIn: function (e, page) {
        // page has left the view
        app.preloader.hide();
      },
    },
  },
  {
    path: "/xacnhan/",
    url: "./pages/xacnhan.html",
    on: {
      pageInit: function () {
        var numpadInline = app.keypad.create({
          inputEl: "#dona-numpad-inline",
          containerEl: "#dona-inline-container",
          toolbar: false,
          valueMaxLength: 6,
          dotButton: false,
          formatValue: function (value) {
            value = value.toString();
            momoPassSym(value.length);
            return (
              "******".substring(0, value.length) +
              "______".substring(0, 6 - value.length)
            );
          },
          on: {
            change(keypad, value) {
              console.log(keypad, value);
              value = value.toString();
              if (value.length === 6) {
                app.dialog.alert(
                  "Thank you! Your passcode is<br><b>" + value + "</b>",
                  function () {
                    // app.views.main.router.back();
                  }
                );
              }
            },
          },
        });
      },
    },
  },
  {
    name: "cashback search",
    path: "/cashback-search/",
    url: "./pages/cashback-search.html",
    options: {
      history: true,
    },
    on: {
      pageInit: function (e, page) {
        cashbackSearch();
      },
      pageAfterOut: function (e, page) {
        // page has left the view
      },
    },
  },
  {
    path: "/example/",
    url: "./pages/example.html",
  },
  {
    path: "/about/",
    url: "./pages/about.html",
  },
  {
    path: "/form/",
    url: "./pages/form.html",
  },
  {
    path: "/dynamic-route/blog/:blogId/post/:postId/",
    componentUrl: "./pages/dynamic-route.html",
  },
  {
    path: "/request-and-load/user/:userId/",
    async: function (routeTo, routeFrom, resolve, reject) {
      // Router instance
      var router = this;

      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = routeTo.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: "Vladimir",
          lastName: "Kharlampidi",
          about: "Hello, i am creator of Framework7! Hope you like it!",
          links: [
            {
              title: "Framework7 Website",
              url: "http://framework7.io",
            },
            {
              title: "Framework7 Forum",
              url: "http://forum.framework7.io",
            },
          ],
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            componentUrl: "./pages/request-and-load.html",
          },
          {
            context: {
              user: user,
            },
          }
        );
      }, 1000);
    },
  },
  // Default route (404 page). MUST BE THE LAST
  {
    path: "(.*)",
    url: "./pages/404.html",
  },
];

function DonaGallery() {
  $$(".js-donation-gallery").on("click", function () {
    myPhotoBrowserPopupDark.open();
  });
  /*=== With Captions ===*/
  var myPhotoBrowserPopupDark = app.photoBrowser.create({
    photos: [
      {
        url:
          "https://static.mservice.io/blogscontents/momo-upload-api-190715170209-636988069298419087.jpg",
        caption:
          "Các bé cùng thả bóng bay, chắp cánh ước mơ và hy vọng trong “Ngày hội hoa hướng dương”",
      },
      {
        url:
          "https://static.mservice.io/blogscontents/momo-upload-api-190715170229-636988069495497917.jpg",
        caption:
          "Các bé tham gia “Ngày hội hoa hướng dương” chuỗi hoạt động nằm trong chương trình “Ước Mơ của Thúy”",
      },
      // This one without caption
      {
        url:
          "https://static.mservice.io/blogscontents/momo-upload-api-190715170252-636988069728133296.jpg",
        caption:
          "Các bé cùng thả bóng bay, chắp cánh ước mơ và hy vọng trong “Ngày hội hoa hướng dương”",
      },
    ],
    theme: "dark",
    type: "standalone",
    backLinkText: "",
  });
}

function momoPassSym(value) {
  var sym = Array.from(document.querySelectorAll(".momo-pass-sym .sym"));
  sym.forEach((item) => item.classList.remove("is-active"));
  sym.forEach(function (item, index) {
    if (index <= value - 1) {
      item.classList.add("is-active");
    }
  });
}

function copyCode() {
  /* Get the text field */
  var copyText = document.getElementById("inputCode");

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

  /* Copy the text inside the text field */
  document.execCommand("copy");
  copyText.blur();

  const noti = document.querySelector(".copy-popover");
  // document.activeElement.blur();

  noti.classList.add("is-active");
  setTimeout(function () {
    noti.classList.remove("is-active");
  }, 3000);
  /* Alert the copied text */
  // alert("Copied the text: " + copyText.value);
}

function contentLoadMore(e) {
  const saleMore = e.querySelector(".sale-cluster-content");
  const saleMoreBtn = e.querySelector(".js-sale-more-btn");
  const saleMoreHeight = saleMore.scrollHeight;
  if (saleMoreHeight > 200) {
    e.classList.add("active");

    saleMoreBtn.addEventListener("click", function () {
      saleMore.style.setProperty("max-height", saleMoreHeight + "px");
      e.classList.remove("active");
    });
  }
}

/* 
var saleMoreCon = Array.from(document.querySelectorAll(".js-sale-more"));
saleMoreCon.forEach(function(e) {
  contentLoadMore(e);
}); */
