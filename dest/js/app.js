var $$ = Dom7;
var T7  = Template7 ;
Framework7.use(Framework7Keypad);
var app = new Framework7({
  root: "#app", // App root element
  name: "My App", // App name
  theme: "ios", // Automatic theme detection
  // App root data
  view: {
    pushState: true,
    iosDynamicNavbar: false,
    xhrCache: true,
    // pushStateSeparator: "",
    // pushStateAnimate: false,
    // preloadPreviousPage: false
  },
  webView: true,
  data: function() {
    return {
      user: {
        firstName: "John",
        lastName: "Doe"
      }
    };
  },
  // App root methods
  methods: {
    helloWorld: function() {
      app.dialog.alert("Hello World!");
    }
  },
  // App routes
  routes: routes,
  // Register service worker
  serviceWorker: {
    path: "/service-worker.js"
  },
  sheet: {
    backdrop: true
  },
  // touch: {
  //   // Enable fast clicks
  //   fastClicks: true,
  // }
});

//Regsiter Helper

T7.registerHelper('times', function(n, block) {
  var accum = '';
  for(var i = 0; i < n; ++i)
      accum += block.fn(i);
  return accum;
});
// Login Screen Demo
$$("#my-login-screen .login-button").on("click", function() {
  var username = $$('#my-login-screen [name="username"]').val();
  var password = $$('#my-login-screen [name="password"]').val();

  // Close login screen
  app.loginScreen.close("#my-login-screen");

  // Alert username and password
  app.dialog.alert("Username: " + username + "<br>Password: " + password);
});
